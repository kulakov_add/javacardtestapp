package memTest;

import javacard.framework.*;
import javacardx.external.*;

public class memTest extends Applet {
	
	// errors
	final static short SW_VERIFICATION_FAILED = 	  0x6300;
	final static short SW_PIN_VERIFICATION_REQUIRED = 0x6301;

	// valid CLA value
	final static byte VALID_CLA = (byte)     	0xB0;
	
	// valid INS values
	final static byte ACTIVATE = (byte)     	0x20;
	final static byte DEACTIVATE = (byte)   	0x30;
	final static byte STORE_DATA = (byte)       0x40;
	final static byte VERIFY = (byte) 			0x50;
	final static byte GET_STATE = (byte)       	0x60;
	
	// pin vars
	byte PIN_TRY_LIMIT;
	final static byte MAX_PIN_SIZE = (byte) 	0x04;
	
	// pin object
	OwnerPIN pin;
	
	// test data
	final static byte DATA_SIZE = (byte) 10;
	byte[] someData;
	
	// state vars
	boolean isInstalled = false;
	boolean dataIsStored = false;
	
	// memory access object
	MemoryAccess oMemAccess;
	
	protected memTest(byte[] bArray, short bOffset, byte bLength) {
		
		try { // since I added this initialization the JCIDE card emulator refuses to make the applet selectable
		oMemAccess = Memory.getMemoryAccessInstance(Memory.MEMORY_TYPE_MIFARE, null, (short)0);
		} catch (ExternalException E) {
			return;
		}
		
		PIN_TRY_LIMIT = (byte) bArray[bOffset]; // pin extraction never worked correctly either
		pin = new OwnerPIN(PIN_TRY_LIMIT, MAX_PIN_SIZE); 
		pin.update(bArray, bOffset, bLength);

		isInstalled = true;
		someData = new byte[DATA_SIZE];
		register();
	}
	
	public static void install(byte[] bArray, short bOffset, byte bLength) {
		new memTest(bArray, (short) (bOffset), bArray[bOffset]);
	}
	
	public void deselect() {
		pin.reset();
	}
	
	public void process(APDU apdu) {
		if (selectingApplet()) {
			return;
		}
		
		byte[] buf = apdu.getBuffer();	
		
		if (buf[ISO7816.OFFSET_CLA] != VALID_CLA)
			ISOException.throwIt(ISO7816.SW_CLA_NOT_SUPPORTED);

		switch (buf[ISO7816.OFFSET_INS]) {
		case ACTIVATE:
			activate(apdu);
			return;
		case DEACTIVATE:
			deactivate(apdu);
			return;
		case STORE_DATA:
			storeData(apdu);
			return;
		case VERIFY:
			verify(apdu);
			return;
		case GET_STATE:
			getState(apdu);
			return;
			
		default:
			ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
		}	
	}
	
	private void activate(APDU apdu) {} // never managed to implement those methods
	
	private void deactivate(APDU apdu) {}

	private void storeData(APDU apdu) {
		
		if (!pin.isValidated()) // data cannot be stored unless pin is validated
			ISOException.throwIt(SW_PIN_VERIFICATION_REQUIRED);
			
		byte[] buf = apdu.getBuffer();

		apdu.setIncomingAndReceive();
		for (byte i = 0; i < buf[ISO7816.OFFSET_LC]; i ++) {
			someData[i] = buf[ISO7816.OFFSET_CDATA + i];
		}		
		dataIsStored = true;
		return;
	}
	
	private void unlock(APDU apdu) {}
	
	private void getState(APDU apdu) { 
		byte[] buf = apdu.getBuffer();
		byte check;
			if (dataIsStored)
				check = (byte) 0x01;
			else
				check = (byte) 0x00; 

			Util.setShort(buf, (short) 0, (short) check);
			try {
				apdu.setOutgoingAndSend((short)0, (short)1);
			} catch (APDUException E) {
				return;
			}
	}
	
	private void verify(APDU apdu) {
		byte[] buffer = apdu.getBuffer();

		byte byteRead = (byte)(apdu.setIncomingAndReceive());

		if (pin.check(buffer, ISO7816.OFFSET_CDATA,byteRead) == false)
			ISOException.throwIt(SW_VERIFICATION_FAILED);
	}
	
}

