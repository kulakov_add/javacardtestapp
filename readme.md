# Java Card Test App

This was supposed to be a Java Card Test App but something went wrong.

### Usage

There are a few thing this applet can do. For instance, to enter a pin you should use
```
/send B050<pin bytes>
```
If pin is correct you can store data to a card using
```
/send B040<data bytes>
```
To check if any data was stored by previous command you can use
```
/send B060
```
It returns 01 if stored and 00 otherwise.

That's pretty much it.

## Built With

This applet was created using JCIDE.